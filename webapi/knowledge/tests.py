import requests
import uuid

API_URL_BASE = 'http://proxy.opensecret.mhnlabs.co.uk/api'
def api_url(path):
    return '{}/{}'.format(API_URL_BASE, path)

def test_fetch_instance_graph():
    req = requests.get(api_url('graph'))
    assert req.ok
    instdata = req.json()
    assert 'nodes' in instdata
    assert 'edges' in instdata
    nodes = instdata['nodes']
    edges = instdata['edges']
    assert len(nodes) > 0
    assert len(edges) > 0
    
    
def test_fetch_schema_graph():
    req = requests.get(api_url('schema/graph'))
    assert req.ok
    instdata = req.json()
    assert 'nodes' in instdata
    assert 'edges' in instdata
    nodes = instdata['nodes']
    edges = instdata['edges']
    assert len(nodes) > 0
    assert len(edges) > 0

def test_create_topic_instance():
    instdata = {
        "keywords":"test;topic;thing",
        "name":"TestTopic",
        "desc":"a test topic",
        "class_name":"Topic"
    }

    req = requests.post(api_url('instances/'), json=instdata)
    assert req.ok
    respdata = req.json()
    assert int(respdata['_id']) > 0
    assert uuid.UUID(respdata['_oid'])
    newprops = respdata['props']
    assert 'keywords' in newprops
    assert 'name' in newprops
    assert 'desc' in newprops
    #assert respdata == dict()
    
def test_fetch_single_topic_instance():
    insts = requests.get(api_url('graph')).json()['nodes']
    topic_instances = [x for x in insts if x['class'] == 'Topic']
    req = requests.get(api_url('instances/{}/'.format(topic_instances[-1]['id'])))
    assert req.ok
    resp = req.json()
    assert resp == {}
    
